{
    inputs = {
      # to fetch gleam 1.2
      nixpkgs.url = "github:NixOS/nixpkgs/master";
    };

    outputs = { self, nixpkgs }:
        let
            lib = nixpkgs.lib;
            systems = ["x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin"];
            forEachSystem = systems: f: lib.genAttrs systems (system: f system);
            forAllSystems = forEachSystem systems;
        in
        {
            devShell = forAllSystems (system:
                with import nixpkgs { inherit system; };
                pkgs.mkShell {
                    buildInputs = with pkgs; [
                        erlang
                        rebar3
                        gleam
                        inotify-tools
                        esbuild
                    ];
                }
            );
        };
}
