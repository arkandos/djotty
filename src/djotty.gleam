import gleam/dict
import gleam/list
import gleam/string

import lustre
import lustre/attribute.{class}
import lustre/element/html.{div, textarea}
import lustre/event.{on_input}

import djotty/attrs
import djotty/inline
import djotty/lexer

pub fn to_html(djot: String) -> String {
  todo
}

pub fn main() {
  let app = lustre.simple(init, update, view)
  let assert Ok(_) = lustre.start(app, "#app", Nil)
  Nil
}

type Model {
  Model(djot: String)
}

fn init(_) {
  Model(djot: "")
}

type Msg {
  DjotChanged(String)
}

fn update(_model: Model, msg) {
  case msg {
    DjotChanged(djot) -> Model(djot: djot)
  }
}

fn view(model: Model) {
  let tokens = lexer.to_tokens(model.djot)
  let ast = to_inlines(tokens, inline.new())
  let rendered = inspect_inlines(ast, 0)

  div([class("grid grid-cols-2 gap-2 font-mono h-screen")], [
    div([class("w-full h-full")], [
      textarea(
        [class("bg-transparent p-4 block w-full h-full"), on_input(DjotChanged)],
        model.djot,
      ),
    ]),
    div([class("bg-gray-600 w-full h-full")], [
      textarea(
        [class("bg-transparent text-gray-300 p-4 block w-full h-full")],
        rendered,
      ),
    ]),
    html.link([
      attribute.href("./priv/static/djotty.css"),
      attribute.rel("stylesheet"),
    ]),
  ])
}

fn inspect(node: inline.Node) -> String {
  case node {
    inline.Container(container, attr) -> {
      case container {
        inline.Emphasis(inlines) -> inspect_container("em", attr, inlines)
        inline.Strong(inlines) -> inspect_container("strong", attr, inlines)
        inline.Superscript(inlines) -> inspect_container("sup", attr, inlines)
        inline.Subscript(inlines) -> inspect_container("sub", attr, inlines)
        inline.Insert(inlines) -> inspect_container("ins", attr, inlines)
        inline.Delete(inlines) -> inspect_container("del", attr, inlines)
        inline.Highlight(inlines) -> inspect_container("mark", attr, inlines)
        inline.Span(inlines) -> inspect_container("span", attr, inlines)

        inline.Image(inlines, target) ->
          inspect_container("img " <> string.inspect(target), attr, inlines)

        inline.Link(inlines, target) ->
          inspect_container("a " <> string.inspect(target), attr, inlines)

        inline.Verbatim(str) -> inspect_verbatim("verbatim", attr, str)

        inline.Math(str, False) -> inspect_verbatim("math", attr, str)

        inline.Math(str, True) -> inspect_verbatim("math_display", attr, str)

        inline.FootnoteReference(str) ->
          inspect_verbatim("footnote_reference", attr, str)

        inline.Autolink(str) -> inspect_verbatim("url", attr, str)
        inline.Email(str) -> inspect_verbatim("email", attr, str)

        inline.Symbol(str) -> inspect_verbatim("symbol", attr, str)
      }
    }
    inline.Softbreak -> ""
    _ -> string.inspect(node)
  }
}

fn inspect_verbatim(name, attr, str) {
  name <> " " <> inspect_attr(attr) <> "\n  " <> string.inspect(str)
}

fn inspect_container(name, attr, inlines) {
  name <> " " <> inspect_attr(attr) <> "\n" <> inspect_inlines(inlines, 2)
}

fn inspect_inlines(nodes: inline.Inlines, indent: Int) -> String {
  let indent = string.repeat(" ", indent)
  nodes
  |> list.flat_map(fn(node) {
    node
    |> inspect
    |> string.split("\n")
  })
  |> list.map(fn(line) { indent <> line })
  |> string.join("\n")
}

fn inspect_attr(attr: attrs.Attributes) -> String {
  attr
  |> dict.to_list
  |> list.map(fn(pair) { pair.0 <> "=" <> string.inspect(pair.1) })
  |> string.join(" ")
}

fn to_inlines(tokens: lexer.Tokens, parser: inline.Parser) -> inline.Inlines {
  case tokens {
    [] -> inline.to_ast(parser)
    _ -> {
      let #(parser, tokens) = inline.parse_line(parser, tokens)
      to_inlines(tokens, parser)
    }
  }
}
