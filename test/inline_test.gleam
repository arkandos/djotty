import gleam/dict
import gleeunit
import gleeunit/should

import djotty/attrs
import djotty/inline
import djotty/lexer

pub fn main() {
  gleeunit.main()
}

pub fn basic_test() {
  expect("hello there", [text("hello there")])
}

pub fn verbatim_test() {
  expect("x ``` hello ``there ``` x", [
    text("x "),
    c(inline.Verbatim(" hello ``there ")),
    text(" x"),
  ])

  expect("`x", [c(inline.Verbatim("x"))])
  expect("`a{i=0}`", [c(inline.Verbatim("a{i=0}"))])

  expect("` ``abc`` `", [c(inline.Verbatim("``abc``"))])

  expect("`.`{unclosed", [c(inline.Verbatim(".")), text("{unclosed")])
}

pub fn math_test() {
  expect("$`abc` def", [c(inline.Math("abc", display: False)), text(" def")])
  expect("$$`abc`", [c(inline.Math("abc", display: True))])
  expect("$`abc", [c(inline.Math("abc", display: False))])
  expect("$```abc```", [c(inline.Math("abc", display: False))])

  expect("$`\\sum_{i=0}^n 2^i`", [
    c(inline.Math("\\sum_{i=0}^n 2^i", display: False)),
  ])
}

pub fn raw_test() {
  expect("`raw`{=format}", [inline.RawInline("raw", "format")])
  expect("`raw`{=format #id}", [
    c(inline.Verbatim("raw")),
    text("{=format #id}"),
  ])
}

pub fn escape_test() {
  expect("\\\"\\*\\ \\a \\\n", [
    text("\"*"),
    inline.NonBreakingSpace,
    text("\\a "),
    inline.Hardbreak,
  ])
}

pub fn autolink_test() {
  expect("<http://example.com?foo=bar&baz=&amp;x2>", [
    c(inline.Autolink("http://example.com?foo=bar&baz=&amp;x2")),
  ])
  expect("<not-a-url>", [text("<not-a-url>")])
}

pub fn email_test() {
  expect("<me@example.com>", [c(inline.Email("me@example.com"))])
}

pub fn sub_sup_test() {
  expect("H~2~O e=mc^2^ test{^two words^}", [
    text("H"),
    c(inline.Subscript([text("2")])),
    text("O e=mc"),
    c(inline.Superscript([text("2")])),
    text(" test"),
    c(inline.Superscript([text("two words")])),
  ])
}

pub fn em_strong_test() {
  expect("_hello *there*_ world", [
    c(inline.Emphasis([text("hello "), c(inline.Strong([text("there")]))])),
    text(" world"),
  ])

  expect("{_abc_}", [c(inline.Emphasis([text("abc")]))])
  expect("{_{_abc_}_}", [
    c(inline.Emphasis([c(inline.Emphasis([text("abc")]))])),
  ])

  expect("*}abc", [text("*}abc")])

  expect("{*{_abc*}", [c(inline.Strong([text("{_abc")]))])

  expect("_This is *strong within* regular emphasis_", [
    c(
      inline.Emphasis([
        text("This is "),
        s(inline.Strong, "strong within"),
        text(" regular emphasis"),
      ]),
    ),
  ])

  expect("_}abc{_", [text("_}abc{_")])

  expect("*not strong *strong*", [
    text("*not strong "),
    s(inline.Strong, "strong"),
  ])

  expect("_ abc _", [text("_ abc _")])

  expect("___", [text("___")])

  expect("__inner_ outer_", [
    c(inline.Emphasis([c(inline.Emphasis([text("inner")])), text(" outer")])),
  ])

  expect("{_ spaces _}", [s(inline.Emphasis, " spaces ")])
}

pub fn mark_test() {
  expect("{=hello=}", [c(inline.Highlight([text("hello")]))])
}

pub fn insert_test() {
  expect("{+hello+}", [c(inline.Insert([text("hello")]))])
  expect("My boss is {-mean-}{+nice+}", [
    text("My boss is "),
    s(inline.Delete, "mean"),
    s(inline.Insert, "nice"),
  ])
}

pub fn quoted_test() {
  expect(" 'a' ", [
    text(" "),
    inline.LeftSingleQuote,
    text("a"),
    inline.RightSingleQuote,
    text(" "),
  ])

  expect("\"dog's breakfast\"", [
    inline.LeftDoubleQuote,
    text("dog"),
    inline.RightSingleQuote,
    text("s breakfast"),
    inline.RightDoubleQuote,
  ])

  expect("\"Hello,\" said the spider. \"'Shelob' is my name.\"", [
    inline.LeftDoubleQuote,
    text("Hello,"),
    inline.RightDoubleQuote,
    text(" said the spider. "),
    inline.LeftDoubleQuote,
    inline.LeftSingleQuote,
    text("Shelob"),
    inline.RightSingleQuote,
    text(" is my name."),
    inline.RightDoubleQuote,
  ])

  expect("'}Tis Socrates' season to be jolly!", [
    inline.RightSingleQuote,
    text("Tis Socrates"),
    inline.RightSingleQuote,
    text(" season to be jolly!"),
  ])

  expect("5\\'11\\\"", [text("5'11\"")])
}

pub fn attributes_test() {
  expect("_a_{#foo .bar baz=\"bim\"}", [
    inline.Container(
      inline.Emphasis([text("a")]),
      dict.from_list([#("id", "foo"), #("class", "bar"), #("baz", "bim")]),
    ),
  ])

  expect("^.^{unclosed", [c(inline.Superscript([text(".")])), text("{unclosed")])

  expect("_abc def_{.a}{.b}{.c} {.d}", [
    inline.Container(
      inline.Emphasis([text("abc def")]),
      dict.from_list([#("class", "a b c")]),
    ),
    text(" "),
  ])
}

pub fn span_test() {
  expect("[hi]{#foo .bar baz=\"bim\"}", [
    inline.Container(
      inline.Span([text("hi")]),
      dict.from_list([#("id", "foo"), #("class", "bar"), #("baz", "bim")]),
    ),
  ])

  expect("[]{.cls}", [
    inline.Container(inline.Span([]), dict.from_list([#("class", "cls")])),
  ])

  expect("[]", [text("[]")])
  expect("[abc]", [text("[abc]")])

  expect("not a [span] {#id}.", [text("not a [span] .")])
}

pub fn inline_link_test() {
  expect("[foobar](url)", [c(inline.Link([text("foobar")], inline.Url("url")))])
  expect("before [text](url) after", [
    text("before "),
    c(inline.Link([text("text")], inline.Url("url"))),
    text(" after"),
  ])
  expect("[outer [inner](i)](o)", [
    c(inline.Link(
      [text("outer "), c(inline.Link([text("inner")], inline.Url("i")))],
      inline.Url("o"),
    )),
  ])

  expect("[](url)", [c(inline.Link([], inline.Url("url")))])
  expect("[text]()", [c(inline.Link([text("text")], inline.Url("text")))])

  expect("[text]({.cls}", [text("[text](")])
  expect("[text](url", [text("[text](url")])
}

pub fn reference_link_test() {
  expect("[foobar][1]", [
    c(inline.Link([text("foobar")], inline.Reference("1"))),
  ])

  expect("[[inner][i]][o]", [
    c(inline.Link(
      [c(inline.Link([text("inner")], inline.Reference("i")))],
      inline.Reference("o"),
    )),
  ])

  expect("[][label]", [c(inline.Link([], inline.Reference("label")))])

  expect("[text][]", [c(inline.Link([text("text")], inline.Reference("text")))])

  expect("[some _text_][]", [
    c(inline.Link(
      [text("some "), c(inline.Emphasis([text("text")]))],
      inline.Reference("some text"),
    )),
  ])
}

pub fn inline_image_test() {
  expect("![foobar](url)", [
    c(inline.Image([text("foobar")], inline.Url("url"))),
  ])
  expect("![](url)", [c(inline.Image([], inline.Url("url")))])
}

pub fn symbol_test() {
  expect(":+1:", [c(inline.Symbol("+1"))])
}

pub fn ellipses_test() {
  expect("...", [inline.Ellipsis])
}

pub fn dashes_test() {
  expect("a---b--c", [
    text("a"),
    inline.EmDash,
    text("b"),
    inline.EnDash,
    text("c"),
  ])

  expect("a----b c------d", [
    text("a"),
    inline.EnDash,
    inline.EnDash,
    text("b c"),
    inline.EmDash,
    inline.EmDash,
    text("d"),
  ])
}

pub fn note_reference_test() {
  expect("[^ref]", [c(inline.FootnoteReference("ref"))])
  expect("text[^foo]. more text", [
    text("text"),
    c(inline.FootnoteReference("foo")),
    text(". more text"),
  ])
}

pub fn precedence_test() {
  expect("_This is *regular_ not strong* emphasis", [
    s(inline.Emphasis, "This is *regular"),
    text(" not strong* emphasis"),
  ])

  expect("*This is _strong* not regular_ emphasis", [
    s(inline.Strong, "This is _strong"),
    text(" not regular_ emphasis"),
  ])

  expect("[Link *](url)", [c(inline.Link([text("Link *")], inline.Url("url")))])

  expect("*Emphasis [*](url)", [s(inline.Strong, "Emphasis ["), text("](url)")])
}

fn text(str) {
  inline.Text(str)
}

fn s(container, str) {
  c(container([text(str)]))
}

fn c(container) {
  inline.Container(container, attrs.new())
}

fn expect(djot, expected) {
  let tokens = lexer.to_tokens(djot)
  let inlines = to_inlines(tokens, inline.new())
  should.equal(inlines, expected)
}

fn to_inlines(tokens, parser) {
  case tokens {
    [] -> inline.to_ast(parser)
    _ -> {
      let #(parser, tokens) = inline.parse_line(parser, tokens)
      to_inlines(tokens, parser)
    }
  }
}
