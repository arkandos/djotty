import gleam/dict.{type Dict}
import gleam/option.{type Option, None, Some}

import djotty/lexer.{type Tokens}

pub type Attributes =
  Dict(String, String)

type ParseResult(a) =
  Result(#(a, Tokens), Nil)

pub fn new() -> Attributes {
  dict.new()
}

pub fn parse_inline(
  tokens: Tokens,
  attrs: Attributes,
) -> ParseResult(Attributes) {
  parse(tokens, None, attrs)
}

pub fn parse_block(
  tokens: Tokens,
  indent: Int,
  attrs: Attributes,
) -> ParseResult(Attributes) {
  parse(tokens, Some(indent), attrs)
}

fn parse(
  tokens: Tokens,
  indent: Option(Int),
  attrs: Attributes,
) -> ParseResult(Attributes) {
  case tokens {
    [lexer.Open(lexer.Brace), ..tokens] -> attributes(tokens, indent, attrs)
    _ -> Error(Nil)
  }
}

fn attributes(
  tokens: Tokens,
  indent: Option(Int),
  attrs: Attributes,
) -> ParseResult(Attributes) {
  case tokens {
    [] -> Error(Nil)
    [lexer.Close(lexer.Brace), ..tokens] -> Ok(#(attrs, tokens))
    [lexer.Spaces(_), ..tokens] -> attributes(tokens, indent, attrs)
    [lexer.Newline, ..tokens] ->
      case indent {
        Some(required) ->
          case tokens {
            [lexer.Spaces(found), ..tokens] if found > required ->
              attributes(tokens, indent, attrs)
            _ -> Error(Nil)
          }
        None -> attributes(tokens, indent, attrs)
      }

    [lexer.Percent, ..tokens] -> attributes(comment(tokens), indent, attrs)

    [lexer.Seq(lexer.Hash, 1), ..tokens] ->
      case simple_attribute_value(tokens, "") {
        Ok(#(id, tokens)) ->
          attributes(tokens, indent, dict.insert(attrs, "id", id))
        Error(Nil) -> attributes(tokens, indent, attrs)
      }

    [lexer.Seq(lexer.Period, 1), ..tokens] ->
      case simple_attribute_value(tokens, "") {
        Ok(#(class, tokens)) ->
          attributes(
            tokens,
            indent,
            dict.update(attrs, "class", fn(existing) {
              case existing {
                None -> class
                Some(existing) -> existing <> " " <> class
              }
            }),
          )
        Error(Nil) -> attributes(tokens, indent, attrs)
      }

    _ -> {
      case simple_attribute_value(tokens, "") {
        Ok(#(key, [lexer.Equal, ..tokens])) ->
          case attribute_value(tokens) {
            Ok(#(value, tokens)) ->
              attributes(tokens, indent, dict.insert(attrs, key, value))
            Error(Nil) -> Error(Nil)
          }
        _ -> Error(Nil)
      }
    }
  }
}

fn attribute_value(tokens: Tokens) {
  case tokens {
    [] -> Error(Nil)
    [lexer.Quote2, ..tokens] -> quoted_attribute_value(tokens, "")
    _ -> simple_attribute_value(tokens, "")
  }
}

fn quoted_attribute_value(tokens, builder) {
  case tokens {
    [] -> Error(Nil)
    [lexer.Quote2, ..tokens] -> Ok(#(builder, tokens))
    [lexer.Close(lexer.BraceQuote2), ..tokens] -> {
      let tokens = [lexer.Close(lexer.Brace), ..tokens]
      Ok(#(builder, tokens))
    }
    [head, ..tail] ->
      quoted_attribute_value(
        tail,
        builder <> lexer.token_to_string(head, False),
      )
  }
}

fn simple_attribute_value(tokens, builder) {
  case tokens {
    // [\w_-]+
    // TODO: this also allows some other non-special chars, like ','
    [lexer.Text(_) as token, ..tokens]
    | [lexer.Seq(lexer.Hyphen, _) as token, ..tokens]
    | [lexer.Underscore as token, ..tokens] ->
      simple_attribute_value(
        tokens,
        builder <> lexer.token_to_string(token, True),
      )

    _ ->
      case builder {
        "" -> Error(Nil)
        _ -> Ok(#(builder, tokens))
      }
  }
}

fn comment(tokens) {
  case tokens {
    [] -> tokens
    [lexer.Close(lexer.Brace), ..] -> tokens
    [lexer.Percent, ..tokens] -> tokens
    [_, ..tokens] -> comment(tokens)
  }
}
