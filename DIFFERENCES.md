### Known differences in behaviour compared to [djot.js](https://github.com/jgm/djot.js)

#### Attributes

 - classes and ids can be chained without the need for whitespace between them, i.e. `#id.a.b`
 - TODO: currently, djotty allows some additional characters inside of keys and class names (everything the lexer recognizes as Text)
 - TODO: Multi-line attributes currently break in nested blocks with line starts

Maybe not breaking out is good? This makes commenting out larger sections containing block structure possible.

This is the case that would break:

```dj
> _x_{.a key="
> value"}
---
  block_quote
    para
      emph class="a" key=" value"
        str text="x"
```

also compare to

```dj
> _x_{.a key="
>
> value"}
---
block_quote
    para
      emph
        str text="x"
      str text="{.a key="
      smart_punctuation type="right_double_quote" text="\""
    para
      str text="value"
      smart_punctuation type="right_double_quote" text="\""
```

#### Verbatim

TODO?: In djotty, there is a single valid escape sequence inside of verbatim, which can be used to include a backtick inside of the verbatim content:

```dj
    `before \` after `
```

results in a single verbatim containing the string ``before ` after``.

#### URLs

djotty handles links slightly differently. None of these changes affect the  tests or examples present in djot.js, but rather change implementation details in uncommon edge-cases. The general goal of those changes is to make parsing around URLs and links more consistent with other features.

 djot.js URL handling is buggy and inconsistent with itself: Using brace-openers results in duplicate braces. Using a link inside of the URL results in the inner URL. Autolinks are parsed entirely differently. Since this should just be a case of normal nesting, I believe that the result should instead be the outer URL:

```dj
[outer](http://[inner](https://djot.net/?{=x))
```

is parsed like this by djot.js:

```
 para
    str text="[outer](http://"
    link destination="https://djot.net/?{{="
      str text="inner"
    str text=")"
```

I believe following the precedence rules of the documentation, this should instead look like this, which is the way djotty parses it instead:

```
 para
    link destination="http://[inner](https://djot.net/?{=)"
      str text="outer"
```

It is inconsistent: Autolinks are parsed using the verbatim parser (ignoring openers/closers and escape sequences), but explicit links are parsed normally, so can contain closers.

```dj
*[test](https://example.com?q=test\)*)
```

parses as

```
  para
    strong
      str text="[test](https://example.com?q=test"
      str text=")"
    str text=")"
```

in djot.js, but djotty instead parses it like this:

```
  para
    strong
      link destination="https://example.com?q=test\\"
        str text="test"
    str text=")"
```


 - In djotty, special characters and escape sequences are ignored inside of links. Links have to end on the same line. (like autolinks or other verbatim content).
 - in djotty, you can use nested parenthesis in links without ending link prematurely. (#74)
 - in djotty, autolinks can contain nested `<` and `>`.


