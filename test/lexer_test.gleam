import gleeunit
import gleeunit/should

import djotty/lexer.{
  Angle, Asterisk, Backtick, BraceHyphen, BracePlus, Caret, Close, Escaped,
  ExclaimBracket, Hardbreak, Hyphen, Nbsp, Open, Period, Pipe, Quote1, Quote2,
  Seq, Spaces, Text, Tilde, Underscore, to_tokens,
}

pub fn main() {
  gleeunit.main()
}

fn test_lex(str, tokens) {
  should.equal(to_tokens(str), tokens)
}

pub fn empty_test() {
  test_lex("", [])
}

pub fn basic_test() {
  test_lex("para w/ some _emphasis_ and *strong*.", [
    Text("para"),
    Spaces(1),
    Text("w/"),
    Spaces(1),
    Text("some"),
    Spaces(1),
    Underscore,
    Text("emphasis"),
    Underscore,
    Spaces(1),
    Text("and"),
    Spaces(1),
    Asterisk,
    Text("strong"),
    Asterisk,
    Seq(Period, 1),
  ])
}

pub fn escape_test() {
  test_lex("\\a", [Text("\\a")])
  test_lex("\\\\a", [Escaped("\\"), Text("a")])
  test_lex("\\.", [Escaped(".")])
  test_lex("\\ ", [Nbsp])
  test_lex("\\{-", [Escaped("{"), Seq(Hyphen, 1)])
}

pub fn hardbreak_test() {
  test_lex("a\\\n", [Text("a"), Hardbreak])
  test_lex("a\\   \n", [Text("a"), Hardbreak])
  test_lex("a\\\t \t  \n", [Text("a"), Hardbreak])
}

pub fn delim_test() {
  test_lex("{-", [Open(BraceHyphen)])
  test_lex("-}", [Close(BraceHyphen)])
  test_lex("{++}", [Open(BracePlus), Close(BracePlus)])
}

pub fn sym_test() {
  test_lex("'*^![<|\"~_", [
    Quote1,
    Asterisk,
    Caret,
    ExclaimBracket,
    Open(Angle),
    Pipe,
    Quote2,
    Tilde,
    Underscore,
  ])

  test_lex("''''", [Quote1, Quote1, Quote1, Quote1])
}

pub fn seq_test() {
  test_lex("`", [Seq(Backtick, 1)])
  test_lex("```", [Seq(Backtick, 3)])
  test_lex("`-.", [Seq(Backtick, 1), Seq(Hyphen, 1), Seq(Period, 1)])
}
