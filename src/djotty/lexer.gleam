import gleam/iterator.{Done, Next}
import gleam/list
import gleam/string

pub type Token {
  Text(String)
  Escaped(String)
  Spaces(Int)
  Newline
  Nbsp
  Hardbreak
  Open(Delimiter)
  Close(Delimiter)
  Asterisk
  Caret
  ExclaimBracket
  Pipe
  Quote1
  Quote2
  Tilde
  Underscore
  Plus
  Equal
  Percent
  Dollar
  DollarDollar
  Seq(Sequence, Int)
  Eof
}

pub type Tokens =
  List(Token)

pub type Delimiter {
  Brace
  BraceAsterisk
  BraceCaret
  BraceEqual
  BraceHyphen
  BracePlus
  BraceTilde
  BraceUnderscore
  Bracket
  BraceQuote1
  BraceQuote2
  Paren
  Angle
}

pub type Sequence {
  Backtick
  Hyphen
  Period
  Hash
  Colon
}

fn eat_eol_whitespace(src) {
  case eat_whitespace(src, 1) {
    #(Newline, src) -> Ok(src)
    _ -> Error(Nil)
  }
}

fn eat_text(src, acc) {
  case string.pop_grapheme(src) {
    Error(Nil) -> #(acc, src)
    Ok(#(chr, rest)) ->
      case is_special(chr) {
        True -> #(acc, src)
        False -> eat_text(rest, acc <> chr)
      }
  }
}

fn eat_whitespace(src, acc) {
  case src {
    " " <> src | "\t" <> src -> eat_whitespace(src, acc + 1)
    "\n" <> src -> #(Newline, src)
    _ -> #(Spaces(acc), src)
  }
}

fn is_special(chr) {
  string.contains("\\[](){}*^=+~_'\"-!<>|:`.%#$\n\t ", chr)
}

fn is_ascii_punctuation(chr) {
  // https://doc.rust-lang.org/std/primitive.char.html#method.is_ascii_punctuation
  string.contains("!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~", chr)
}

fn try_eat_close_brace(src, kind, delimiter) {
  case src {
    "}" <> src -> #(Close(delimiter), src)
    _ -> #(kind, src)
  }
}

fn eat_seq(src, seq, count) {
  case src {
    // special case to handle -} always as BraceHyphen
    "-}" <> _ -> #(Seq(seq, count), src)

    _ ->
      case string.pop_grapheme(src) {
        Ok(#(first, rest)) ->
          case first == sequence_to_string(seq) {
            True -> eat_seq(rest, seq, count + 1)
            False -> #(Seq(seq, count), src)
          }

        Error(Nil) -> #(Seq(seq, count), src)
      }
  }
}

pub fn pop_token(src: String) -> #(Token, String) {
  // since to_graphemes is implemented as (roughly) unfold(pop_grapheme), I assume
  // that string patterns are actually faster than to_graphemes and list patterns
  case src {
    "" -> #(Eof, src)
    "\n" <> src -> #(Newline, src)
    " " <> src | "\t" <> src -> eat_whitespace(src, 1)
    "[" <> src -> #(Open(Bracket), src)
    "]" <> src -> #(Close(Bracket), src)
    "(" <> src -> #(Open(Paren), src)
    ")" <> src -> #(Close(Paren), src)
    "}" <> src -> #(Close(Brace), src)
    "<" <> src -> #(Open(Angle), src)
    ">" <> src -> #(Close(Angle), src)
    "|" <> src -> #(Pipe, src)
    "%" <> src -> #(Percent, src)

    "$" <> src ->
      case src {
        "$" <> src -> #(DollarDollar, src)
        _ -> #(Dollar, src)
      }

    "*" <> src -> try_eat_close_brace(src, Asterisk, BraceAsterisk)
    "^" <> src -> try_eat_close_brace(src, Caret, BraceCaret)
    "~" <> src -> try_eat_close_brace(src, Tilde, BraceTilde)
    "_" <> src -> try_eat_close_brace(src, Underscore, BraceUnderscore)
    "'" <> src -> try_eat_close_brace(src, Quote1, BraceQuote1)
    "+" <> src -> try_eat_close_brace(src, Plus, BracePlus)
    "=" <> src -> try_eat_close_brace(src, Equal, BraceEqual)
    "\"" <> src -> try_eat_close_brace(src, Quote2, BraceQuote2)

    // for those patterns, there is no Sym
    "-}" <> src -> #(Close(BraceHyphen), src)
    "![" <> src -> #(ExclaimBracket, src)
    "!" <> src -> #(Text("!"), src)

    "`" <> src -> eat_seq(src, Backtick, 1)
    "." <> src -> eat_seq(src, Period, 1)
    "-" <> src -> eat_seq(src, Hyphen, 1)
    ":" <> src -> eat_seq(src, Colon, 1)
    "#" <> src -> eat_seq(src, Hash, 1)

    "{" <> src ->
      case src {
        "*" <> src -> #(Open(BraceAsterisk), src)
        "^" <> src -> #(Open(BraceCaret), src)
        "=" <> src -> #(Open(BraceEqual), src)
        "-" <> src -> #(Open(BraceHyphen), src)
        "+" <> src -> #(Open(BracePlus), src)
        "~" <> src -> #(Open(BraceTilde), src)
        "_" <> src -> #(Open(BraceUnderscore), src)
        "'" <> src -> #(Open(BraceQuote1), src)
        "\"" <> src -> #(Open(BraceQuote2), src)
        _ -> #(Open(Brace), src)
      }

    "\\" <> src ->
      case eat_eol_whitespace(src) {
        Ok(src) -> #(Hardbreak, src)
        Error(Nil) ->
          case string.pop_grapheme(src) {
            Error(Nil) -> #(Text("\\"), src)
            Ok(#(" ", src)) -> #(Nbsp, src)
            Ok(#("\t", src)) -> eat_whitespace(src, 1)
            Ok(#(chr, src)) ->
              case is_ascii_punctuation(chr) {
                True -> #(Escaped(chr), src)
                False -> #(Text("\\" <> chr), src)
              }
          }
      }

    _ -> {
      let #(text, src) = eat_text(src, "")
      #(Text(text), src)
    }
  }
}

pub fn to_tokens(src: String) -> List(Token) {
  src
  |> iterator.unfold(fn(src) {
    case pop_token(src) {
      #(Eof, _) -> Done
      #(token, src) -> Next(element: token, accumulator: src)
    }
  })
  |> iterator.to_list
}

pub fn to_string(tokens: List(Token), verbatim: Bool) -> String {
  use acc, token <- list.fold(tokens, "")
  acc <> token_to_string(token, verbatim)
}

pub fn token_to_string(token: Token, verbatim: Bool) -> String {
  case token {
    Text(str) -> str
    Escaped(str) if verbatim -> "\\" <> str
    Escaped(str) -> str
    Spaces(count) -> string.repeat(" ", count)
    Newline -> "\n"
    Nbsp -> "\\ "
    Hardbreak if verbatim -> "\\\n"
    Hardbreak -> "\n"

    Open(Brace) -> "{"
    Open(BraceAsterisk) -> "{*"
    Open(BraceCaret) -> "{^"
    Open(BraceEqual) -> "{="
    Open(BraceHyphen) -> "{-"
    Open(BracePlus) -> "{+"
    Open(BraceTilde) -> "{~"
    Open(BraceUnderscore) -> "{_"
    Open(Bracket) -> "["
    Open(BraceQuote1) -> "{'"
    Open(BraceQuote2) -> "{\""
    Open(Paren) -> "("
    Open(Angle) -> "<"

    Close(Brace) -> "}"
    Close(BraceAsterisk) -> "*}"
    Close(BraceCaret) -> "^}"
    Close(BraceEqual) -> "=}"
    Close(BraceHyphen) -> "-}"
    Close(BracePlus) -> "+}"
    Close(BraceTilde) -> "~}"
    Close(BraceUnderscore) -> "_}"
    Close(Bracket) -> "]"
    Close(BraceQuote1) -> "'}"
    Close(BraceQuote2) -> "\"}"
    Close(Paren) -> ")"
    Close(Angle) -> ">"

    Asterisk -> "*"
    Caret -> "^"
    ExclaimBracket -> "!["
    Pipe -> "|"
    Quote1 -> "'"
    Quote2 -> "\""
    Tilde -> "~"
    Underscore -> "_"
    Dollar -> "$"
    DollarDollar -> "$$"
    Plus -> "+"
    Equal -> "="
    Percent -> "%"

    Seq(seq, count) -> string.repeat(sequence_to_string(seq), count)
    Eof -> ""
  }
}

fn sequence_to_string(seq: Sequence) -> String {
  case seq {
    Period -> "."
    Hyphen -> "-"
    Backtick -> "`"
    Colon -> ":"
    Hash -> "#"
  }
}
