import filepath
import gleam/io
import gleam/list
import gleam/string
import gleeunit
import gleeunit/should
import simplifile

import djotty

const test_directory = "test/cases"

type Test {
  Test(path: String, djot: String, html: String)
}

pub fn main() {
  gleeunit.main()
}

pub fn integration_test_disabled() {
  let tests = load_tests()
  let failures = {
    use failures, testcase <- list.fold(tests, 0)

    let html = djotty.to_html(testcase.djot)
    case html == testcase.html {
      True -> {
        io.print_error(".")
        failures
      }

      False -> {
        io.print_error("F")
        io.print_error("\nTest failed: " <> testcase.path)
        io.print_error("\nInput: " <> string.inspect(testcase.djot))
        io.print_error("\nExpected: " <> string.inspect(testcase.html))
        io.print_error("\nActual: " <> string.inspect(html))
        io.print_error("\n\n")
        failures + 1
      }
    }
  }

  should.equal(failures, 0)
}

fn load_tests() {
  let assert Ok(tests) = simplifile.read_directory(test_directory)
  use file_name <- list.flat_map(tests)

  let path = filepath.join(test_directory, file_name)
  let assert Ok(content) = simplifile.read(path)
  let lines = string.split(content, "\n")
  parse_test(lines, path, [])
}

fn parse_test(lines, path, examples) {
  case drop_empty_lines(lines) {
    [] -> list.reverse(examples)
    [delim, ..lines] -> {
      case string.starts_with(delim, "`") {
        True -> {
          let #(djot, lines) = collect_until(lines, ".", "")
          let #(html, lines) = collect_until(lines, delim, "")
          parse_test(lines, path, [Test(path, djot, html), ..examples])
        }

        False -> {
          parse_test(lines, path, examples)
        }
      }
    }
  }
}

fn collect_until(lines, delim, acc) {
  case lines {
    [] -> #(acc, [])

    [line, ..lines] if line == delim -> {
      #(acc, lines)
    }

    [line, ..lines] -> {
      collect_until(lines, delim, acc <> line <> "\n")
    }
  }
}

fn drop_empty_lines(lines) {
  case lines {
    ["", ..rest] -> drop_empty_lines(rest)
    _ -> lines
  }
}
