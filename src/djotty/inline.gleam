import gleam/int
import gleam/list
import gleam/string

import djotty/attrs.{type Attributes}
import djotty/lexer.{type Delimiter, type Tokens}

pub type Node {
  Text(String)
  Hardbreak
  Softbreak
  NonBreakingSpace
  Ellipsis
  EnDash
  EmDash
  LeftSingleQuote
  RightSingleQuote
  LeftDoubleQuote
  RightDoubleQuote
  Container(Container, Attributes)
  RawInline(text: String, format: String)
}

pub type Container {
  Emphasis(Inlines)
  Strong(Inlines)
  Superscript(Inlines)
  Subscript(Inlines)
  Insert(Inlines)
  Delete(Inlines)
  Highlight(Inlines)
  Span(Inlines)
  Verbatim(String)
  Math(String, display: Bool)
  FootnoteReference(String)
  Symbol(String)

  // image allows Inlines even though formatting is stripped later...
  Image(Inlines, target: LinkTarget)
  Link(Inlines, target: LinkTarget)
  Autolink(String)
  Email(String)
}

pub type LinkTarget {
  Url(String)
  Reference(String)
}

pub opaque type Parser {
  Parser(idx: Int, curr: Buffer, stack: Stack, saw_whitespace: Bool)
}

type Inlines =
  List(Node)

type Buffer =
  List(#(Int, Node))

type Stack =
  List(#(lexer.Token, Int, StackMarker))

type StackMarker {
  MarkerUnused
  MarkerLinkOrSpan
  MarkerImageAlt
}

pub fn new() -> Parser {
  Parser(idx: 0, curr: [], stack: [], saw_whitespace: True)
}

pub fn to_ast(parser: Parser) -> Inlines {
  use acc, el <- list.fold(parser.curr, [])
  prepend_node(acc, el.1)
}

pub fn parse_line(parser: Parser, tokens: Tokens) -> #(Parser, Tokens) {
  case tokens {
    [] -> #(parser, tokens)
    [lexer.Newline, ..tokens] -> {
      let parser =
        Parser(..prepend_buffer(parser, Softbreak), saw_whitespace: True)
      #(parser, tokens)
    }

    [lexer.Hardbreak, ..tokens] -> {
      let parser =
        Parser(..prepend_buffer(parser, Hardbreak), saw_whitespace: True)
      parse_line(parser, tokens)
    }

    [lexer.Nbsp, ..tokens] -> {
      let parser =
        Parser(..prepend_buffer(parser, NonBreakingSpace), saw_whitespace: True)
      parse_line(parser, tokens)
    }

    [lexer.Spaces(_) as token, ..tokens] -> {
      let parser = Parser(..prepend_token(parser, token), saw_whitespace: True)
      parse_line(parser, tokens)
    }

    [lexer.Open(lexer.Brace) as token, ..rest] ->
      case parser.curr {
        [#(idx, Container(container, attrs)), ..curr] ->
          case attrs.parse_inline(tokens, attrs) {
            Ok(#(attrs, tokens)) -> {
              let curr = [#(idx, Container(container, attrs)), ..curr]
              parse_line(Parser(..parser, curr: curr), tokens)
            }

            Error(Nil) -> parse_line(prepend_token(parser, token), rest)
          }
        _ ->
          // still parse to skip over the attributes if we cannot add them
          case attrs.parse_inline(tokens, attrs.new()) {
            Ok(#(_, tokens)) -> parse_line(parser, tokens)
            Error(Nil) -> parse_line(prepend_token(parser, token), rest)
          }
      }

    [lexer.Seq(lexer.Period, count), ..tokens] -> {
      let idx = parser.idx
      let ellipsis = list.repeat(#(idx, Ellipsis), count / 3)
      let period = #(idx, Text(string.repeat(".", count % 3)))
      let curr = list.append([period, ..ellipsis], parser.curr)
      let parser =
        Parser(..parser, idx: idx + 1, curr: curr, saw_whitespace: False)
      parse_line(parser, tokens)
    }

    [lexer.Seq(lexer.Hyphen, hyphens), ..tokens] -> {
      let idx = parser.idx

      let #(ems, ens, ones) = case hyphens % 3 == 0, hyphens % 2 == 0 {
        True, _ -> #(hyphens / 3, 0, 0)
        _, True -> #(0, hyphens / 2, 0)
        False, False -> {
          let ems = int.max(0, { hyphens - 2 } / 3)
          let hyphens = hyphens - ems * 3
          #(ems, hyphens / 2, hyphens % 2)
        }
      }

      let curr =
        list.concat([
          [#(idx, Text(string.repeat("-", ones)))],
          list.repeat(#(idx, EnDash), ens),
          list.repeat(#(idx, EmDash), ems),
          parser.curr,
        ])

      let parser =
        Parser(..parser, idx: idx + 1, curr: curr, saw_whitespace: False)

      parse_line(parser, tokens)
    }

    [lexer.Underscore as token, ..tokens] ->
      inline_pair(Emphasis, token, tokens, parser)

    [lexer.Asterisk as token, ..tokens] ->
      inline_pair(Strong, token, tokens, parser)

    [lexer.Caret as token, ..tokens] ->
      inline_pair(Superscript, token, tokens, parser)

    [lexer.Tilde as token, ..tokens] ->
      inline_pair(Subscript, token, tokens, parser)

    [lexer.Quote1, ..tokens] ->
      quote_pair(parser, LeftSingleQuote, RightSingleQuote, tokens)

    [lexer.Quote2, ..tokens] ->
      quote_pair(parser, LeftDoubleQuote, RightDoubleQuote, tokens)

    [lexer.Open(lexer.BraceQuote1), ..tokens] ->
      parse_line(prepend_buffer(parser, LeftSingleQuote), tokens)

    [lexer.Open(lexer.BraceQuote2), ..tokens] ->
      parse_line(prepend_buffer(parser, LeftDoubleQuote), tokens)

    [lexer.DollarDollar, lexer.Seq(lexer.Backtick, _) as end, ..tokens] -> {
      let #(text, tokens) = verbatim(tokens, end, "")
      let node = Container(Math(text, display: True), attrs.new())
      parse_line(prepend_buffer(parser, node), tokens)
    }

    [lexer.Dollar, lexer.Seq(lexer.Backtick, _) as end, ..tokens] -> {
      let #(text, tokens) = verbatim(tokens, end, "")
      let node = Container(Math(text, display: False), attrs.new())
      parse_line(prepend_buffer(parser, node), tokens)
    }

    [lexer.Seq(lexer.Backtick, _) as end, ..tokens] -> {
      let #(text, tokens) = verbatim(tokens, end, "")
      case tokens {
        [
          lexer.Open(lexer.BraceEqual),
          lexer.Text(format),
          lexer.Close(lexer.Brace),
          ..tokens
        ] -> {
          parse_line(prepend_buffer(parser, RawInline(text, format)), tokens)
        }

        _ -> {
          let node = Container(Verbatim(text), attrs.new())
          parse_line(prepend_buffer(parser, node), tokens)
        }
      }
    }

    [lexer.Open(lexer.Bracket), lexer.Caret, ..tokens] -> {
      case delimited_verbatim(tokens, lexer.Bracket, 1, "") {
        Ok(#(text, tokens)) -> {
          let text = string.trim(text)
          let node = Container(FootnoteReference(text), attrs.new())
          parse_line(prepend_buffer(parser, node), tokens)
        }

        Error(Nil) ->
          parser
          |> prepend_token(lexer.Open(lexer.Bracket))
          |> prepend_token(lexer.Caret)
          |> parse_line(tokens)
      }
    }

    [lexer.Open(lexer.Angle) as token, ..tokens] -> {
      case delimited_verbatim(tokens, lexer.Angle, 1, "") {
        Ok(#(url, rest)) -> {
          // text cannot contain any space, cannot be empty,
          // has to contain a : or a @ that is not the first character
          // https://github.com/jgm/djot.js/blob/main/src/inline.ts#L242
          let has_space = string.contains(url, " ")
          let has_colon = url |> string.drop_left(1) |> string.contains(":")
          let has_at = url |> string.drop_left(1) |> string.contains("@")

          case has_space, has_colon, has_at {
            False, True, _ ->
              parser
              |> prepend_buffer(Container(Autolink(url), attrs.new()))
              |> parse_line(rest)

            False, _, True ->
              parser
              |> prepend_buffer(Container(Email(url), attrs.new()))
              |> parse_line(rest)

            _, _, _ -> parse_line(prepend_token(parser, token), tokens)
          }
        }

        Error(Nil) -> parse_line(prepend_token(parser, token), tokens)
      }
    }

    [lexer.ExclaimBracket as token, ..tokens] -> {
      let parser =
        Parser(
          ..prepend_token(parser, token),
          stack: [
            #(lexer.Close(lexer.Bracket), parser.idx, MarkerImageAlt),
            ..parser.stack
          ],
        )
      parse_line(parser, tokens)
    }

    [lexer.Open(lexer.Bracket) as token, ..tokens] -> {
      let parser =
        Parser(
          ..prepend_token(parser, token),
          stack: [
            #(lexer.Close(lexer.Bracket), parser.idx, MarkerLinkOrSpan),
            ..parser.stack
          ],
        )
      parse_line(parser, tokens)
    }

    [lexer.Seq(lexer.Colon, count) as token, ..tokens] -> {
      case symbol(tokens, "") {
        Ok(#(text, tokens)) -> {
          let idx = parser.idx
          let curr = [
            #(idx, Container(Symbol(text), attrs.new())),
            #(idx, Text(string.repeat(":", count - 1))),
            ..parser.curr
          ]

          let parser =
            Parser(..parser, idx: idx + 1, curr: curr, saw_whitespace: False)
          parse_line(parser, tokens)
        }

        Error(Nil) -> parse_line(prepend_token(parser, token), tokens)
      }
    }

    [lexer.Open(delim), ..tokens] ->
      // NOTE: this pushes some unnessesary stuff onto the stack but its fine
      parse_line(open_delim(parser, delim), tokens)

    [lexer.Close(lexer.BraceAsterisk as delim), ..tokens] ->
      parse_line(close_delim(parser, Strong, delim), tokens)

    [lexer.Close(lexer.BraceUnderscore as delim), ..tokens] ->
      parse_line(close_delim(parser, Emphasis, delim), tokens)

    [lexer.Close(lexer.BraceCaret as delim), ..tokens] ->
      parse_line(close_delim(parser, Superscript, delim), tokens)

    [lexer.Close(lexer.BraceTilde as delim), ..tokens] ->
      parse_line(close_delim(parser, Subscript, delim), tokens)

    [lexer.Close(lexer.BraceEqual as delim), ..tokens] ->
      parse_line(close_delim(parser, Highlight, delim), tokens)

    [lexer.Close(lexer.BracePlus as delim), ..tokens] ->
      parse_line(close_delim(parser, Insert, delim), tokens)

    [lexer.Close(lexer.BraceHyphen as delim), ..tokens] ->
      parse_line(close_delim(parser, Delete, delim), tokens)

    [lexer.Close(lexer.BraceQuote1), ..tokens] ->
      parse_line(prepend_buffer(parser, RightSingleQuote), tokens)

    [lexer.Close(lexer.BraceQuote2), ..tokens] ->
      parse_line(prepend_buffer(parser, RightDoubleQuote), tokens)

    // span, link, image
    [lexer.Close(lexer.Bracket) as token, ..tokens] ->
      case pop_inline_container(token, parser.stack), tokens {
        Ok(#(start, MarkerLinkOrSpan, stack)), [lexer.Open(lexer.Brace), ..] -> {
          // we will parse the span attributes on the next loop of parse_line
          let curr = compress_inlines(Span, parser.curr, start, [])
          let parser =
            Parser(..parser, curr: curr, stack: stack, saw_whitespace: False)
          parse_line(parser, tokens)
        }

        Ok(#(start, MarkerLinkOrSpan, stack)),
          [lexer.Open(lexer.Paren as delim), ..tokens]
        -> image_or_link(parser, delim, tokens, start, stack, Link, Url)

        Ok(#(start, MarkerLinkOrSpan, stack)),
          [lexer.Open(lexer.Bracket as delim), ..tokens]
        -> image_or_link(parser, delim, tokens, start, stack, Link, Reference)

        Ok(#(start, MarkerImageAlt, stack)),
          [lexer.Open(lexer.Paren as delim), ..tokens]
        -> image_or_link(parser, delim, tokens, start, stack, Image, Url)

        Ok(#(start, MarkerImageAlt, stack)),
          [lexer.Open(lexer.Bracket as delim), ..tokens]
        -> image_or_link(parser, delim, tokens, start, stack, Image, Reference)

        _, _ -> parse_line(prepend_token(parser, token), tokens)
      }

    [token, ..tokens] -> {
      parse_line(prepend_token(parser, token), tokens)
    }
  }
}

fn image_or_link(parser: Parser, delim, tokens, start, stack, container, target) {
  case delimited_verbatim(tokens, delim, 1, "") {
    Ok(#(url_or_ref, tokens)) -> {
      let compress = fn(inlines) {
        let url_or_ref = case string.trim(url_or_ref) {
          "" ->
            string.trim({
              // if url_or_ref is empty, fallback to a text-only view of the inlines
              use str, node <- list.fold(inlines, "")
              str <> text_content(node)
            })

          url_or_ref -> url_or_ref
        }

        container(inlines, target(url_or_ref))
      }

      let curr = compress_inlines(compress, parser.curr, start, [])
      let parser =
        Parser(..parser, curr: curr, stack: stack, saw_whitespace: False)
      parse_line(parser, tokens)
    }

    Error(Nil) -> {
      let parser =
        parser
        // image_or_link is always called after we consumed the `]`
        |> prepend_token(lexer.Close(lexer.Bracket))
        |> prepend_token(lexer.Open(delim))

      let parser = Parser(..parser, stack: stack)
      parse_line(parser, tokens)
    }
  }
}

fn open_delim(parser: Parser, delim: Delimiter) {
  let idx = parser.idx
  let parser = prepend_token(parser, lexer.Open(delim))
  Parser(
    ..parser,
    stack: [#(lexer.Close(delim), idx, MarkerUnused), ..parser.stack],
  )
}

fn close_delim(
  parser: Parser,
  container: fn(Inlines) -> Container,
  delim: Delimiter,
) {
  case pop_inline_container(lexer.Close(delim), parser.stack) {
    Ok(#(start, _, stack)) -> {
      let curr = compress_inlines(container, parser.curr, start, [])
      Parser(..parser, curr: curr, stack: stack, saw_whitespace: False)
    }
    Error(Nil) -> prepend_token(parser, lexer.Close(delim))
  }
}

fn inline_pair(
  container: fn(Inlines) -> Container,
  token: lexer.Token,
  tokens: Tokens,
  parser: Parser,
) {
  let can_open = !sees_whitespace(tokens)
  let can_close = !parser.saw_whitespace
  case can_close {
    True ->
      case close_pair(parser, container, token) {
        Ok(parser) -> parse_line(parser, tokens)
        Error(Nil) ->
          case can_open {
            True -> open_pair(parser, token, tokens)
            False -> parse_line(prepend_token(parser, token), tokens)
          }
      }
    False ->
      case can_open {
        True -> open_pair(parser, token, tokens)
        False -> parse_line(prepend_token(parser, token), tokens)
      }
  }
}

fn open_pair(parser: Parser, token, tokens) {
  let parser =
    Parser(
      ..prepend_token(parser, token),
      stack: [#(token, parser.idx, MarkerUnused), ..parser.stack],
    )

  // once we open, we prefer to open as many in a row, so ___ parses as 3 open
  // instead of <em>_</em>
  case tokens {
    [other, ..tokens] if other == token -> open_pair(parser, other, tokens)
    _ -> parse_line(parser, tokens)
  }
}

fn close_pair(parser: Parser, container, token) {
  case pop_inline_container(token, parser.stack) {
    Ok(#(start, _, stack)) -> {
      let idx = parser.idx
      let curr = compress_inlines(container, parser.curr, start, [])
      Ok(Parser(idx: idx + 1, curr: curr, stack: stack, saw_whitespace: False))
    }

    Error(Nil) -> Error(Nil)
  }
}

fn quote_pair(parser: Parser, left, right, tokens) {
  let can_close = !parser.saw_whitespace
  case can_close {
    True -> parse_line(prepend_buffer(parser, right), tokens)
    False -> {
      let parser =
        Parser(
          ..prepend_buffer(parser, left),
          saw_whitespace: parser.saw_whitespace,
        )
      parse_line(parser, tokens)
    }
  }
}

fn verbatim(tokens: Tokens, end: lexer.Token, builder) {
  case tokens {
    [] -> #(builder, tokens)
    [lexer.Newline, ..] -> #(builder, tokens)

    [token, ..tokens] if token == end -> #(builder, tokens)

    // If the content starts or ends with a backtick character, a single space is removed between the opening or closing backticks and the content.
    [lexer.Spaces(1), lexer.Seq(lexer.Backtick, _) as token, ..tokens]
      if builder == "" && token != end
    -> {
      let builder = builder <> lexer.token_to_string(token, True)
      verbatim(tokens, end, builder)
    }

    [lexer.Seq(lexer.Backtick, _) as token, lexer.Spaces(1), token2, ..tokens]
      if token != end && token2 == end
    -> {
      let text = builder <> lexer.token_to_string(token, True)
      #(text, tokens)
    }

    [token, ..tokens] -> {
      let builder = builder <> lexer.token_to_string(token, True)
      verbatim(tokens, end, builder)
    }
  }
}

fn delimited_verbatim(tokens: Tokens, delim: lexer.Delimiter, counter, result) {
  case tokens {
    [] | [lexer.Newline, ..] -> Error(Nil)

    [lexer.Open(open) as token, ..tokens] if open == delim -> {
      let result = result <> lexer.token_to_string(token, True)
      delimited_verbatim(tokens, delim, counter + 1, result)
    }

    [lexer.Close(close) as token, ..tokens] if close == delim ->
      case counter == 1 {
        True -> Ok(#(result, tokens))
        False -> {
          let result = result <> lexer.token_to_string(token, True)
          delimited_verbatim(tokens, delim, counter - 1, result)
        }
      }

    [token, ..tokens] -> {
      let result = result <> lexer.token_to_string(token, True)
      delimited_verbatim(tokens, delim, counter, result)
    }
  }
}

fn symbol(tokens, builder) {
  case tokens {
    [lexer.Seq(lexer.Colon, count), ..tokens] -> {
      let tokens = case count == 1 {
        True -> tokens
        False -> [lexer.Seq(lexer.Colon, count - 1), ..tokens]
      }

      Ok(#(builder, tokens))
    }
    [lexer.Text(_) as token, ..tokens]
    | [lexer.Underscore as token, ..tokens]
    | [lexer.Seq(lexer.Hyphen, _) as token, ..tokens]
    | [lexer.Plus as token, ..tokens] ->
      symbol(tokens, builder <> lexer.token_to_string(token, True))

    _ -> Error(Nil)
  }
}

fn sees_whitespace(tokens: Tokens) {
  case tokens {
    // NOTE: we don't see whitespace on Hardbreak or Nbsp, since in those cases,
    // we actually "see" the blackslash! at least that is what djot.js does.
    [] | [lexer.Spaces(_), ..] | [lexer.Newline, ..] -> True
    _ -> False
  }
}

fn compress_inlines(
  container: fn(Inlines) -> Container,
  curr: Buffer,
  start: Int,
  acc: Inlines,
) -> Buffer {
  case curr {
    [#(idx, elem), ..rest] if idx > start ->
      compress_inlines(container, rest, start, prepend_node(acc, elem))
    [#(idx, _), ..rest] if idx == start -> {
      // skip the "start" token - this is the start marker that we do no longer want as text
      [#(start, Container(container(acc), attrs.new())), ..rest]
    }
    _ -> [#(start, Container(container(acc), attrs.new())), ..curr]
  }
}

fn pop_inline_container(
  token: lexer.Token,
  stack: Stack,
) -> Result(#(Int, StackMarker, Stack), Nil) {
  case stack {
    [] -> Error(Nil)
    [#(top, start, marker), ..stack] if top == token ->
      Ok(#(start, marker, stack))
    [_, ..stack] -> pop_inline_container(token, stack)
  }
}

fn prepend_token(parser: Parser, token: lexer.Token) -> Parser {
  prepend_buffer(parser, Text(lexer.token_to_string(token, False)))
}

fn prepend_buffer(parser: Parser, node: Node) -> Parser {
  Parser(
    ..parser,
    idx: parser.idx + 1,
    curr: [#(parser.idx, node), ..parser.curr],
    saw_whitespace: False,
  )
}

fn prepend_node(xs: Inlines, x: Node) -> Inlines {
  case x, xs {
    Text(""), _ -> xs
    Text(text1), [Text(text2), ..xs] -> [Text(text1 <> text2), ..xs]
    _, _ -> [x, ..xs]
  }
}

pub fn text_content(node: Node) -> String {
  case node {
    Text(str) -> str
    Hardbreak | Softbreak -> "\n"
    NonBreakingSpace -> " "
    Ellipsis -> "..."
    EnDash -> "--"
    EmDash -> "---"
    LeftSingleQuote | RightSingleQuote -> "'"
    LeftDoubleQuote | RightDoubleQuote -> "\""

    Container(container, _) ->
      case container {
        Emphasis(inlines)
        | Strong(inlines)
        | Superscript(inlines)
        | Subscript(inlines)
        | Insert(inlines)
        | Delete(inlines)
        | Highlight(inlines)
        | Span(inlines)
        | Image(inlines, _)
        | Link(inlines, _) -> {
          use str, node <- list.fold(inlines, "")
          str <> text_content(node)
        }

        Verbatim(str)
        | Math(str, _)
        | FootnoteReference(str)
        | Autolink(str)
        | Email(str) -> str

        Symbol(_) -> ""
      }

    RawInline(str, _) -> str
  }
}
