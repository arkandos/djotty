import gleam/dict
import gleeunit
import gleeunit/should

import djotty/attrs.{parse_inline}
import djotty/lexer.{to_tokens}

pub fn main() {
  gleeunit.main()
}

pub fn empty_test() {
  let tokens = to_tokens("{}")
  let assert Ok(#(attrs, tokens)) = parse_inline(tokens, attrs.new())
  should.equal(tokens, [])
  should.equal(attrs, attrs.new())
}

pub fn class_id_test() {
  test_attr("{.some_class #some-id}", [
    #("id", "some-id"),
    #("class", "some_class"),
  ])
  test_attr("{.a .b}", [#("class", "a b")])
  test_attr("{#a #b}", [#("id", "b")])
}

pub fn value_simple_test() {
  test_attr("{attr1=val1 attr2=val2}", [#("attr1", "val1"), #("attr2", "val2")])
}

pub fn value_quoted_test() {
  test_attr("{attr0=\"val0\" attr1=\"val1\"}", [
    #("attr0", "val0"),
    #("attr1", "val1"),
  ])

  test_attr("{#id .class style=\"color: red\"}", [
    #("id", "id"),
    #("class", "class"),
    #("style", "color: red"),
  ])
}

pub fn value_newline_test() {
  test_attr("{attr=\"abc\ndef\"}", [#("attr", "abc\ndef")])
}

pub fn comment_test() {
  test_attr("{%}", [])
  test_attr("{%%}", [])
  test_attr("{ % abc % }", [])
  test_attr("{ .some_class % #some_id }", [#("class", "some_class")])
  test_attr("{ .some_class % abc % #some_id}", [
    #("id", "some_id"),
    #("class", "some_class"),
  ])
}

pub fn escape_test() {
  test_attr("{attr=\"with escaped \\~ char\"}", [
    #("attr", "with escaped ~ char"),
  ])
  test_attr("{key=\"quotes \\\" should be escaped\"}", [
    #("key", "quotes \" should be escaped"),
  ])
  test_attr("{attr=\"with\\\\backslash\"}", [#("attr", "with\\backslash")])
  test_attr("{attr=\"with many backslashes\\\\\\\\\"}", [
    #("attr", "with many backslashes\\\\"),
  ])
  test_attr("{attr=\"\\\\escaped backslash at start\"}", [
    #("attr", "\\escaped backslash at start"),
  ])
  test_attr("{attr=\"do not \\escape\"}", [#("attr", "do not \\escape")])
  test_attr("{attr=\"\\backslash at the beginning\"}", [
    #("attr", "\\backslash at the beginning"),
  ])
}

pub fn djotjs_test() {
  let tokens =
    to_tokens(
      "{a=b #ident
        .class
        key=val1
        .class key2=\"val two \\\" ok\"}abc",
    )

  let assert Ok(#(attrs, tokens)) = parse_inline(tokens, attrs.new())

  should.equal(tokens, [lexer.Text("abc")])
  should.equal(
    attrs,
    dict.from_list([
      #("id", "ident"),
      #("class", "class class"),
      #("a", "b"),
      #("key", "val1"),
      #("key2", "val two \" ok"),
    ]),
  )
}

fn test_attr(str, expected) {
  let assert Ok(#(attrs, tokens)) = parse_inline(to_tokens(str), attrs.new())
  should.equal(tokens, [])
  should.equal(attrs, dict.from_list(expected))
}
