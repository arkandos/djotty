# djotty

[![Package Version](https://img.shields.io/hexpm/v/djotty)](https://hex.pm/packages/djotty)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/djotty/)

A work-in-progress [djot](https://djot.net/) parser for Gleam.

```sh
gleam add djotty
```
```gleam
import djotty

pub fn main() {
  // TODO: An example of the project in use
}
```

Further documentation can be found at <https://hexdocs.pm/djotty>.

## Development

```sh
gleam run   # Run the project
gleam test  # Run the tests
gleam shell # Run an Erlang shell
```
